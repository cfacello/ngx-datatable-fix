"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/* tslint:disable:variable-name */
exports.MouseEvent = ((typeof window !== 'undefined' && window) || global).MouseEvent;
exports.KeyboardEvent = ((typeof window !== 'undefined' && window) || global).KeyboardEvent;
exports.Event = ((typeof window !== 'undefined' && window) || global).Event;
//# sourceMappingURL=events.js.map